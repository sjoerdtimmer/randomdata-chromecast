from __future__ import print_function
import time
import pychromecast
from threading import Timer,Thread
from pprint import pprint

# DEBUG = True

class MyChromecastThread(Thread):
	def __init__(self):
		Thread.__init__(self)
		
	def run(self):
		while True:
			print("connecting to \"Chromecast\"")
			self.cast = pychromecast.get_chromecast(friendly_name="RandomCast")
			if self.cast is None:
				print("failed to connect")
				time.sleep(5)
				continue
			print("connected to \"Chromecast\"")
			# self.cast.reboot()
			self.mc = self.cast.media_controller
			self.cast.register_handler(self.mc)

			# DEBUGGING ONLY:
			self.cast.start_app("42697F72")

			while True:
				pprint(self.cast.status)
				if(self.cast.status is not None and self.cast.status.app_id == "E8C28D3C"): # if backdrop
					print("starting custom app...")
					self.cast.start_app("42697F72")
					print("DONE!")
				time.sleep(1)

	def printstats(self):
		pprint(self.cast.status)
		pprint(self.mc.status)
		print("")


class CheckerThread(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.cast = pychromecast.get_chromecast(friendly_name="Chromecast")

	def run(self):
		while True:
			pprint(self.cast.status)
			time.sleep(1)



if __name__ == "__main__":
	MyChromecastThread().start()
	# CheckerThread().start()


	
	# self.cast = pychromecast.get_chromecast(friendly_name="Chromecast")
	# self.mc = self.cast.media_controller
	# self.cast.register_handler(self.mc)
	# while(True):
		# self.printstats()
		# time.sleep(1)
