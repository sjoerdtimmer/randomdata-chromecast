#! /usr/bin/env python

import webserver
import ircclient
import caststarter

mywebserver = webserver.MyHTTPThread()
myircclient = ircclient.MyIRCThread()
myccclient  = caststarter.MyChromecastThread()

myircclient.register_callback(mywebserver.add_message)
mywebserver.set_userlist_callable(myircclient.bot.get_userlist)

myircclient.start()
mywebserver.start()
myccclient.start()
