#! /usr/bin/env python

from http.server import BaseHTTPRequestHandler, HTTPServer
from http import HTTPStatus
from threading import Thread
import json
import os
from pprint import pprint
import select
import time


plainfiles = [
	"/hud.png",
	"/fan.png",
	"/diagonal.png",
	"/hud1-blur.png",
	"/jquery-2.1.1.min.js",
	"/castreceiver.html",
	"/beeping.mp3",
	"/beeping_short.mp3",
	"/raphael.js",
	"/rotator.gif",
	"/rotator2.gif",
	"/anonymous.png",
	"/clock_green.png",
	"/clock_blue.png",
	"/clock_orange.png"
]


class MyRequestHandler(BaseHTTPRequestHandler):
	def setMainContents(self,contents):
		self.contents = contents

	
	def serveFile(self,filename):
		self.send_response(200,"OK")
		self.end_headers()
		chunksize = 1024
		with open('castreceiver'+filename, "rb") as f:
			while True:
				chunk = f.read(chunksize)
				if chunk:
					self.wfile.write(chunk)            
				else:
					break

	def do_GET(self):
		if self.path in plainfiles:
			self.serveFile(self.path)
		elif self.path == "/":
			self.serveFile("/castreceiver.html")
		elif self.path == "/allmessages.json":
			print("serving messages cache")
			self.send_response(200,"OK")
			self.end_headers()
			self.wfile.write(json.dumps(self.server.messagecache).encode("utf-8"))
		elif self.path == "/userlist.json":
			print("serving userlist")
			self.send_response(200,"OK")
			self.end_headers()
			self.wfile.write(json.dumps(self.server.get_userlist()).encode("utf-8"))
		elif self.path == "/newmessages.json":
			print("serving new messages")
			self.send_response(200,"OK")
			self.end_headers()
			self.wfile.write(json.dumps(self.server.messages).encode("utf-8"))
			self.server.messages = []
		


class MyHTTPThread(Thread):
	
	def __init__(self):
		Thread.__init__(self)
		self.server = HTTPServer(('',8000),MyRequestHandler)
		self.server.messages = []
		self.server.messagecache = []
		# if not os.path.exists("messagecache.json"):
		# 	with open('messagecache.json','w+') as f:
		# 		json.dump([],f)	
		# with open('messagecache.json','r') as f:
			# self.server.messagecache = json.load(f)
		
	def run(self):
		self.server.serve_forever()

	def set_userlist_callable(self,func):
		self.server.get_userlist = func


	def add_message(self,t,isaction,nick,msg):
		self.server.messages.append({'time':t,'isaction':isaction,'nick':nick,'msg':msg})
		self.server.messagecache.append({'time':t,'isaction':isaction,'nick':nick,'msg':msg})
		# with open('messagecache.json','w+') as f:
		# 	json.dump(self.server.messagecache,f)

if __name__ == "__main__":
	myserver = MyHTTPThread()
	myserver.set_userlist_callable(lambda: [])
	myserver.start()