#! /usr/bin/env python
import logging

logging.basicConfig(level=logging.INFO)


import irc.bot
import irc.strings
from irc.client import ip_numstr_to_quad, ip_quad_to_numstr
from pprint import pprint
import time
from threading import Thread

log = logging.getLogger("client")
log.setLevel(logging.INFO)

server  = "chat.freenode.net"
port    = 6667
# channel = "#botwars"
# channel = "#themba"
channel = "#randomdata"
nick    = "randomcast"
# passwd  = "s3cr3t"

# TODO:
# * fancy background (with animations?)
# optionally:
# * respond to on_topic
# * respond to on_action




class RandomCastIRCBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname,reconnection_interval=5)
        self.channel = channel
        self.publish = [print]
        
    def on_topic(self,c,e):
        print("on_topic:")
        pprint(e)

    
    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        print("connected to server")
        c.join(self.channel)

    def on_join(self,c,e):
        print("joined channel")

    def get_userlist(self):
        # return ["@[com]buster","aczid     ","miep_     ","scarybarry","zarya   ","@ChanServ   ","Ardillo   ","FriedZombie","mrngm     ","SpiderWebz","zwamkat_","@DrWhax     ","bar       ","groente    ","Nemoriety ","Stijn     ","@fish___    ","brainsick5","jawsper    ","polyfloyd ","Stitch1   ","@jeroenh    ","BugBlue   ","jelmer     ","potatomas ","themba    ","@SYNNACK    ","carrumba  ","Kartoffel  ","RSpliet   ","themba__  ","@zkyp       ","effractur ","MavJS      ","scarybar1y","Will-Do   "]
        if channel in self.channels:
            pprint(list(self.channels[channel].users()))
            return list(self.channels[channel].users())
        else:
            return []

    def on_privmsg(self, c, e):
        # print(e.source.nick)
        c.privmsg(e.source.nick,"I'm a bot, I cannot reply. Ask themba for info")


    def register_callback(self,func):
        self.publish.append(func)

    def on_pubmsg(self, c, e):
        print("pubmsg: %d: %s: %s"%(time.time(),e.source.nick,e.arguments[0]))
        for publisher in self.publish:
            publisher(time.time(),False,e.source.nick,e.arguments[0])

    def on_action(self, c, e):
        print("action: %d: %s: %s"%(time.time(),e.source.nick,e.arguments[0]))
        for publisher in self.publish:
            publisher(time.time(),True,e.source.nick,e.arguments[0])






class MyIRCThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.bot = RandomCastIRCBot(channel, nick, server, port)

    def register_callback(self,func):
        self.bot.register_callback(func)

    def run(self):
        self.bot.start()



if __name__ == "__main__":
    MyIRCThread().start()

